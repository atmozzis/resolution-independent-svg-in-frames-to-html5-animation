#include "svgcompiler_cli.h"

bool Compile_SVG(const int FPS, char* WORKINGDIR)
{
	// Current directory is "."
	//char * WORKINGDIR;
	//if (argc == 1) { WORKINGDIR = "."; }
	//else { WORKINGDIR = argv[0]; }
	//cout << "Current Working Dir: " << WORKINGDIR << "*" << endl;

	// Check FPS
	if(FPS<=0 || FPS >60)
	{
		/* FPS out of range */
		return false;
	}

	string file_list;
	file_list = get_svg_from_directory(WORKINGDIR);
	if (file_list.length() == 0)
	{
		/* Could not find SVG in directory */
		return false;
	}

	string svg_animation_output = "";
	string svg_animation_output_filename = "";
	int framecnt = 0;
	int MILLISECONDSPERFRAME	= 1000 / FPS;
	int FRAMEOVERLAP			= 16 + MILLISECONDSPERFRAME;	// milliseconds

	while(file_list.length() > 0)
	{
		// filelist format = "filename1.svg;filename2.svg;filename3.svg"
		framecnt++;
		int delim = file_list.find(";");
		string svg_f = file_list.substr(0,delim); file_list.erase(0,delim+1);
		string svg_content = get_file_contents(svg_f);

		smatch svg_match;
		if(regex_search(svg_content, svg_match, regex(SVG_GET_REGEX)))
		{
			svg_content = svg_match[0];
		}

		if(svg_content.length() <= 0)
		{
			//cout << "No SVG element in " << svg_f << "!" << endl;
		}
		else
		{
			// FOR FIRST FRAME
			if(svg_animation_output_filename.length() <= 0)
			{
				// grab filename
				svg_animation_output_filename = svg_f;

				// get svg element
				smatch svg_element_start_regex_match;
				regex_search(svg_content, svg_element_start_regex_match, regex(SVG_ELEMENT_START_REGEX));

				// remove svg open tag
				svg_content.erase(0,svg_element_start_regex_match[0].length());

				// start <g> tag
				ostringstream ss;
				ss << "\n<g display=\"none\">";

				// remove svg close tag
				string fmt = "";
				regex_replace(ostreambuf_iterator<char>(ss), svg_content.begin(), svg_content.end(), regex(SVG_ELEMENT_END_REGEX), fmt);

				// add <animate/> and </g>
				// Opera will flickr if previous frame is cleared before next frame. Display current frame for mspf + 16ms (min limit for 6fps).
				// Chrome and Opera does not recognize fill="remove" or keyTimes. Use two seprate animate event.
				// If SMIL is fully supported, use only one animate element with fill="remove" and duration="FRAMEOVERLAP"
				ss << "<animate id=\"FRAME" << framecnt << "\" attributeName=\"display\" attributeType=\"CSS\" ";
				ss << "begin=\"0ms;LASTFRAME.begin+" << MILLISECONDSPERFRAME << "ms\" values=\"inline\"/>\n";
				ss << "<animate id=\"FRAME" << framecnt << "END\" attributeName=\"display\" attributeType=\"CSS\" ";
				ss << "begin=\"" << FRAMEOVERLAP << "ms;LASTFRAMEEND.begin+" << MILLISECONDSPERFRAME << "ms\" values=\"none\"/>\n</g>\n";
				svg_animation_output += ss.str();
			}
			// FOR LAST FRAME
			else if(file_list.length() <= 0)
			{
				string fmt = "";
				string buff1 = regex_replace(svg_content, regex(SVG_ELEMENT_START_REGEX), fmt);
				ostringstream ss;
				ss << "\n<g display=\"none\">" << regex_replace(buff1, regex(SVG_ELEMENT_END_REGEX), fmt);
				ss << "<animate id=\"LASTFRAME\" attributeName=\"display\" attributeType=\"CSS\" ";
				ss << "begin=\"FRAME" << framecnt-1 << ".begin+" << MILLISECONDSPERFRAME << "ms\" values=\"inline\"/>\n";
				ss << "<animate id=\"LASTFRAMEEND\" attributeName=\"display\" attributeType=\"CSS\" ";
				ss << "begin=\"FRAME" << framecnt-1 << "END.begin+" << MILLISECONDSPERFRAME << "ms\" values=\"none\"/>\n</g>\n";
				svg_animation_output += ss.str();
			}
			else
			{
				string fmt = "";
				string buff1 = regex_replace(svg_content, regex(SVG_ELEMENT_START_REGEX), fmt);
				ostringstream ss;
				ss << "\n<g display=\"none\">" << regex_replace(buff1, regex(SVG_ELEMENT_END_REGEX), fmt);
				ss << "<animate id=\"FRAME" << framecnt << "\" attributeName=\"display\" attributeType=\"CSS\" ";
				ss << "begin=\"FRAME" << framecnt-1 << ".begin+" << MILLISECONDSPERFRAME << "ms\" values=\"inline\"/>\n";
				ss << "<animate id=\"FRAME" << framecnt << "END\" attributeName=\"display\" attributeType=\"CSS\" ";
				ss << "begin=\"FRAME" << framecnt-1 << "END.begin+" << MILLISECONDSPERFRAME << "ms\" values=\"none\"/>\n</g>\n";
				svg_animation_output += ss.str();
			}
		}
	}

	if(svg_animation_output.length() > 0)
	{
		// finalize svg_animation_output
		svg_animation_output.insert(0,get_svg_header());
		svg_animation_output.append(get_svg_script_footer(MILLISECONDSPERFRAME));

		create_folder("./animated_output");
		ostringstream ss;
		ss << "./animated_output/" << svg_animation_output_filename.substr(0,svg_animation_output_filename.length()-4) << "_" << time(NULL) << ".svg";
		if(write_to_file(ss.str(),svg_animation_output))
		{
			// Success
			return true;
		}
	}
	return false;
}