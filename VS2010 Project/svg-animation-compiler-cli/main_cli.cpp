#include <cstdlib>
#include "svgcompiler_cli.h"

int main(int argc, char *argv[])
{
	// svg-animation-compiler.exe					=> execute in current directory, 24FPS
	// svg-animation-compiler.exe /WORKINGDIR		=> execute in directory, 24FPS
	// svg-animation-compiler.exe /WORKINGDIR /FPS	=> execute in directory, specified FPS (0-60)
	
	char * WORKINGDIR = ".";
	int FPS = 24;

	if		(argc == 1)	{ }
	else if	(argc == 2)	{ WORKINGDIR = argv[0]; }
	else if (argc == 3)	{ WORKINGDIR = argv[0]; FPS = atoi(argv[1]); }
	else				{ return -1; }

	bool result = Compile_SVG(FPS, WORKINGDIR);
	return result ? 0 : -1;
}