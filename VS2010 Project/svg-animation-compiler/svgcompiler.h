#ifndef CONVERTER_H
#define CONVERTER_H

#include <iostream>
#include <string>
#include <regex>
#include <ctime>
#include <sstream> 
#include <iterator>
#include "helper.h"
using namespace std;

#define SVG_GET_REGEX "<[^\\S]*svg[^\\S][^>]*>[^]*<[^\\S]*/[^\\S]*svg[^\\S]*[^>]*>"
#define SVG_ELEMENT_START_REGEX "^<[^\\S]*svg[^\\S][^>]*>"
#define SVG_ELEMENT_END_REGEX "<[^\\S]*/[^\\S]*svg[^\\S]*[^>]*>$"
#define ENDL "\n"

bool Compile_SVG(int FPS, char* WORKINGDIR);

#endif