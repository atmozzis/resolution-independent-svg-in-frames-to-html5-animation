#include "ui_getfps.h"

int main(int argc, char *argv[])
{
	Fl_Window* win = make_ui_getfps();
	win->show();

	// Enable multi-thread support by locking from the main
	// thread.  Fl::wait() and Fl::run() call Fl::unlock() and
	// Fl::lock() as needed to release control to the child threads
	// when it is safe to do so...
	Fl::lock();
	return Fl::run();
}