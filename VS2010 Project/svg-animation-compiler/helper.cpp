#include "helper.h"

string get_svg_from_directory(const char *dirname)
{	
	dirent** filelist;
	string strfilelist = "";					// "filename1.svg;filename2.svg;filename3.svg;"
	if(fl_filename_isdir(dirname)!=0)
	{
		int cntFile = fl_filename_list(dirname, &filelist);

		for(int cnti=0; cnti<cntFile; cnti++)
		{
			string filename =  string(filelist[cnti]->d_name);
			if(filename.length() > 4 && filename.compare(filename.length()-4, filename.length(), ".svg") == 0)
			{
				strfilelist += filename + ";";
			}
		}

		fl_filename_free_list(&filelist,cntFile);
	}
	return strfilelist;
}

string get_file_contents(const  string filename)
{
	string contents = "";
	try
	{
		ifstream in(filename,  ios::in |  ios::binary);
		if (in)
		{
			in.seekg(0,  ios::end);
			contents.resize(in.tellg());
			in.seekg(0,  ios::beg);
			in.read(&contents[0], contents.size());
			in.close();
		}
	}
	catch ( exception& e)
	{
		cout << "Unable to open file: " << filename <<  endl;
		cout << e.what() <<  endl;
		contents = "";
	}
	return contents;
}

bool write_to_file (const string filename, const string content)
{
	ofstream file (filename);
	if (file.is_open())
	{
		file << content;
		file.close();
		cout << "Written to : " << filename << endl;
		return true;
	}
	else
	{
		cout << "Unable to write to : " << filename << endl;
	}
	return false;
}

bool create_folder(string dirname)
{
	if(fl_mkdir(dirname.c_str(), 0777))
	{
		return true;
	}
	else if(errno != EEXIST)
	{
		return true;
	}
	else
	{
		return false;
	}
}

string get_svg_header()
{
	return ""
		"<?xml version=\"1.0\" standalone=\"no\"?>"
		"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">"
		"<!-- saved from url=(0014)about:internet -->"
		"<svg width=\"100%\" height=\"100%\" version=\"1.1\" baseProfile=\"full\" "
		"xmlns=\"http://www.w3.org/2000/svg\" "
		"xmlns:xlink=\"http://www.w3.org/1999/xlink\" "
		"xmlns:cacani=\"http://www.cacani.org/namespaces/cacani\">"
		;
}

string get_svg_script_footer(int MILLISECONDSPERFRAME)
{
	string footer = ""
		"\n<script type=\"text/javascript\">"
		"\n//<![CDATA["
		"\n/**"
		"\n * Copyright 2012 Helder Magalhaes"
		"\n * MIT and GPL Licenses"
		"\n *"
		"\n * Determine if SMIL is supported in order to inject the FakeSmile script only when necessary."
		"\n * Measured average execution time (in IE9 and Fx12) was only a few milliseconds."
		"\n *"
		"\n * Usage: place the script next to \"smil.user.js\" and insert it *last* into the document."
		"\n *"
		"\n * Sample usage in an SVG document:"
		"\n * <svg>"
		"\n *   <elements ...>"
		"\n *   <script type=\"text/ecmascript\" xlink:href=\"path/to/smil.check.js\" />"
		"\n * </svg>"
		"\n *"
		"\n * Sample usage in an (X)HTML document:"
		"\n * <html>"
		"\n *   <elements ...>"
		"\n *   <script type=\"text/javascript\" src=\"path/to/smil.check.js\"></script>"
		"\n * </html>"
		"\n */"
		"\n// wrap everything in a function call in order to avoid variable scoping issues and also to allow cleanup when done"
		"\n(function(){"
		"\n// for profiling only - uncomment this and the latest few lines when curious"
		"\n//var start = (new Date()).getTime();"
		"\nvar svgns = \"http://www.w3.org/2000/svg\";"
		"\ntry{"
		"\n  if(!document.implementation.hasFeature(\"http://www.w3.org/TR/SVG11/feature#SVG-animation\", \"1.1\")){"
		"\n    // even when the implementation doesn't claim to fully support SMIL animation, it may support parts of it"
		"\n    // (for example, Gecko has been adding SMIL features since version 2 (Firefox 4), so we should test each feature separately)"
		"\n    // Firefox still fails (as of version 15a1) and apparently will keep failing to implement animateColor"
		"\n    // (https://bugzilla.mozilla.org/show_bug.cgi?id=436296)"
		"\n    // therefore we need to find a reliable way of detecting support - Modernizr seems to have found a nice way of doing it"
		"\n    // https://github.com/Modernizr/Modernizr/issues/35"
		"\n    // https://github.com/Modernizr/Modernizr/blob/master/modernizr.js#L731"
		"\n    // NOTE: evaluation order is important as it impacts performance"
		"\n    // (so we should follow statistics to find out the best order for this list - currently it's based on my own experience)"
		"\n    var smilTags = [\"animate\", \"animateTransform\", \"animateMotion\", \"animateColor\", \"set\"];"
		"\n    var currTag, objName;"
		"\n    for(var i = 0; i < smilTags.length; ++i){"
		"\n      currTag = smilTags[i];"
		"\n      // DOM element name is a camel-case version of the tag name"
		"\n      // (SVGAnimateElement, SVGSetElement, etc.)"
		"\n      objName = currTag.charAt(0).toUpperCase();"
		"\n      objName += currTag.substring(1, currTag.length);"
		"\n      // THINK: which approach one is faster? will it worth to reverse this logic? profile!"
		"\n      // (document.createElementNS + string search vs. using document.getElementsByTagNameNS directly)"
		"\n      // NOTE: forcing cast to string is needed for compatibility with ASV"
		"\n      if(("" + document.createElementNS(svgns, currTag)).indexOf(objName, 11) !== 11){ "
		"\n        // NOTE: 11 = \"[object SVG\".length"
		"\n        // user agent doesn't support this feature"
		"\n        // throw \"FakeSmile potentially required due to lack of support for: \" + currTag;"
		"\n        // deeper validation and further optimization: check if the document actually contains any tags of the unsupported type"
		"\n        // NOTE: this can only be performed when the script is inserted *last* into the document"
		"\n        if(document.getElementsByTagNameNS(svgns, currTag).length){"
		"\n          // at least one element needs support from FakeSmile"
		"\n          throw \"FakeSmile required due to: \" + currTag;"
		"\n        }"
		"\n      }"
		"\n    }"
		"\n  }"
		"\n  var timesheetns = \"http://www.w3.org/2007/07/SMIL30/Timesheets\";"
		"\n  var smil3ns = \"http://www.w3.org/ns/SMIL30\";"
		"\n  if(!document.implementation.hasFeature(timesheetns, \"1.0\")){"
		"\n    // test things which are not namespace-aware first due to support for HTML"
		"\n    // In non-XML markup languages, the link element can be used to reference an external timesheet document"
		"\n    // http://www.w3.org/TR/timesheets/#smilTimesheetsNS-Elements-Timesheet"
		"\n    var linkElems = document.getElementsByTagName(\"link\");"
		"\n    var linkEle;"
		"\n    for(var i = 0; i < linkElems.length; ++i){"
		"\n      linkEle = linkElems.item(i);"
		"\n      // perform similar check to one made in smil.user"
		"\n      if((linkEle.getAttribute(\"rel\") === \"timesheet\") && (linkEle.getAttribute(\"type\") === \"application/smil+xml\")){"
		"\n        throw \"FakeSmile required due to: link\";"
		"\n      }"
		"\n    }"
		"\n    // THINK: can we use an approach similar to the one used for SMIL in SVG above?"
		"\n    // (creating an element and checking if the implementation supports it)"
		"\n    // check if the document actually contains any tags of the unsupported type"
		"\n    if(document.getElementsByTagNameNS(smil3ns, \"timesheet\").length || document.getElementsByTagNameNS(timesheetns, \"timesheet\").length){"
		"\n      // at least one element needs support from FakeSmile"
		"\n      throw \"FakeSmile required due to: timesheet\";"
		"\n    }"
		"\n  }"
		"\n}catch(exp){  "
		"\n  // debug only - uncomment these lines to know which particular element and/or cause is triggering FakeSmile to load"
		"\n  /*"
		"\n  // NOTE: in IE, console object is only available when Developer tools are open"
		"\n  if(window.console && console.log){"
		"\n    console.log(exp);"
		"\n  }else{"
		"\n    alert(exp);"
		"\n  }"
		"\n  */"
		"\n  document.documentElement.setAttribute(\"smiling\", \"needed\");"
		"\n}"
		"\n// for profiling only - uncomment these and the \"var start\" line above when curious"
		"\n/*"
		"\nvar loadTime = \"Took \" + ((new Date()).getTime() - start) + \"ms to perform SMIL check\";"
		"\n// NOTE: in IE, console object is only available when Developer tools are open"
		"\nif(window.console && console.log){"
		"\n  console.log(loadTime);"
		"\n}else{"
		"\n  alert(loadTime);"
		"\n}"
		"\n*/"
		"\n})();"
		"\n//]]>"
		"\n</script>"
		"\n"
		"\n<script type=\"text/javascript\">"
		"\n//<![CDATA["
		"\n(function(){"
		"\n// http://paulirish.com/2011/requestanimationframe-for-smart-animating/"
		"\n// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating"
		"\n// requestAnimationFrame polyfill by Erik Moller. fixes from Paul Irish and Tino Zijdel"
		"\n// MIT license"
		"\n(function() {"
		"\nvar lastTime = 0;"
		"\nvar vendors = ['ms', 'moz', 'webkit', 'o'];"
		"\nfor(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {"
		"\nwindow.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];"
		"\nwindow.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']"
		"\n|| window[vendors[x]+'CancelRequestAnimationFrame'];"
		"\n}"
		"\nif (!window.requestAnimationFrame)"
		"\nwindow.requestAnimationFrame = function(callback, element) {"
		"\nvar currTime = new Date().getTime();"
		"\nvar timeToCall = Math.max(0, 16 - (currTime - lastTime));"
		"\nvar id = window.setTimeout(function() { callback(currTime + timeToCall); },"
		"\ntimeToCall);"
		"\nlastTime = currTime + timeToCall;"
		"\nreturn id;"
		"\n};"
		"\nif (!window.cancelAnimationFrame)"
		"\nwindow.cancelAnimationFrame = function(id) {"
		"\nclearTimeout(id);"
		"\n};"
		"\n}());"
		"\n})();"
		"\n//]]>"
		"\n</script>"
		"\n"
		"\n<script type=\"text/javascript\">"
		"\n//<![CDATA["
		"\n(function(){"
		"\nif (document.documentElement.getAttribute(\"smiling\")!=\"needed\")"
		"\n{"
		"\n  // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)"
		"\n  var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;"
		"\n  // Firefox 1.0+"
		"\n  var isFirefox = typeof InstallTrigger !== 'undefined';"
		"\n  // At least Safari 3+: \"[object HTMLElementConstructor]\""
		"\n  var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;"
		"\n  // Chrome 1+"
		"\n  var isChrome = !!window.chrome && !isOpera;"
		"\n  // At least IE6"
		"\n  var isIE = /*@cc_on!@*/false || !!document.documentMode;"
		"\n  if(isSafari)"
		"\n  {"
		"\n    //window.alert(\"Safari uses Javascript animation by default: bug-report(http://bugs.webkit.org/show_bug.cgi?id=63727)\");"
		"\n    document.documentElement.setAttribute(\"smiling\", \"needed\");"
		"\n  }"
		"\n  else if(isChrome || isOpera)"
		"\n  {"
		"\n    //window.alert(\"Chrome and Opera Browser Engines render SVG animation very poorly. Using Javascript animation by default.\");"
		"\n    document.documentElement.setAttribute(\"smiling\", \"needed\");"
		"\n  }"
		"\n  else"
		"\n  {"
		"\n    //window.alert(\"This browser supports SVG animation feature natively.\");"
		"\n    return;"
		"\n  }"
		"\n}"
		"\nelse"
		"\n{"
		"\n  //window.alert(\"This browser does not support SVG animation feature natively. Javascript will be used to animate and it may" "degrade performance significantly\");"
		"\n  // see more details here: http://caniuse.com/#feat=svg-smil"
		"\n}"
		"\nvar removelist = document.getElementsByTagName(\"animate\");"
		"\nvar svgframes = document.querySelectorAll(\"svg>g\");"
		"\nvar framecount = svgframes.length;"
		"\nvar spf = <<<<<MILLISECONDSPERFRAME>>>>>;       // milliseconds"
		"\nvar curframe = -1;  // un-initialized"
		"\nvar last;           // un-initialized"
		"\nfunction svginit()"
		"\n{"
		"\n  // Remove animate Elements (Safari 5 and Opera 19 Bug Fix)"
		"\n  for (var cnt = removelist.length - 1; cnt >= 0; cnt--)"
		"\n  { removelist[cnt].parentNode.removeChild(removelist[cnt]); }"
		"\n  curframe = 0;"
		"\n  svgdraw();"
		"\n  last = new Date().getTime();"
		"\n  svgloop();"
		"\n}"
		"\nfunction svgdraw(NoOfFrames)"
		"\n{"
		"\n  svgframes[curframe].setAttribute(\"display\",\"none\");"
		"\n  NoOfFrames = NoOfFrames || 1;"
		"\n  curframe ++;"
		"\n  curframe %= framecount;"
		"\n  svgframes[curframe].setAttribute(\"display\",\"inline\");"
		"\n}"
		"\nfunction svgloop()"
		"\n{"
		"\n  requestAnimationFrame(svgloop);"
		"\n  var now = new Date().getTime();"
		"\n  var deltaTime = now - last;"
		"\n  NoOfFrames = deltaTime / spf;"
		"\n  if(NoOfFrames > 1)"
		"\n  {"
		"\n    svgdraw(NoOfFrames);"
		"\n    last = now - (deltaTime % spf);"
		"\n  }"
		"\n}"
		"\n// everything starts functioning here"
		"\nsvginit();"
		"\n})();"
		"\n//]]>"
		"\n</script>"
		"\n</svg>"
		;
	string mspf_pointer = "<<<<<MILLISECONDSPERFRAME>>>>>";
	string::size_type mspf_pos = footer.find(mspf_pointer);
	if (mspf_pos!=string::npos)
	{
		string mspf = to_string(static_cast<long long>(MILLISECONDSPERFRAME));
		footer.replace(mspf_pos,mspf_pointer.length(),mspf);
	}
	return footer;
}