#ifndef HELPER_H
#define HELPER_H

#include <iostream>
#include <string>
#include <fstream>
#include <cerrno>
#include <FL/filename.h>
#include <FL/fl_utf8.h>
#include <errno.h>
using namespace std;

string get_svg_from_directory (const char *dirname);
string get_file_contents(const  string filename);
bool write_to_file (const string filename, const string content);
bool create_folder(string dirname);
string get_svg_header();
string get_svg_script_footer(int MILLISECONDSPERFRAME);

#endif