var NAVTREE =
[
  [ "FLTK 1.3.2", "index.html", [
    [ "FLTK Programming Manual", "index.html", [
      [ "Preface", "preface.html", null ],
      [ "Introduction to FLTK", "intro.html", null ],
      [ "FLTK Basics", "basics.html", null ],
      [ "Common Widgets and Attributes", "common.html", null ],
      [ "Designing a Simple Text Editor", "editor.html", null ],
      [ "Drawing Things in FLTK", "drawing.html", null ],
      [ "Handling Events", "events.html", null ],
      [ "Adding and Extending Widgets", "subclassing.html", null ],
      [ "Using OpenGL", "opengl.html", null ],
      [ "Programming with FLUID", "fluid.html", null ],
      [ "Advanced FLTK", "advanced.html", null ],
      [ "Unicode and UTF-8 Support", "unicode.html", null ],
      [ "FLTK Enumerations", "enumerations.html", null ],
      [ "GLUT Compatibility", "glut.html", null ],
      [ "Forms Compatibility", "forms.html", null ],
      [ "Operating System Issues", "osissues.html", null ],
      [ "Migrating Code from FLTK 1.0 to 1.1", "migration_1_1.html", null ],
      [ "Migrating Code from FLTK 1.1 to 1.3", "migration_1_3.html", null ],
      [ "Developer Information", "development.html", null ],
      [ "Software License", "license.html", null ],
      [ "Example Source Code", "examples.html", null ],
      [ "FAQ (Frequently Asked Questions)", "FAQ.html", null ]
    ] ],
    [ "Related Pages", "pages.html", [
      [ "Todo List", "todo.html", null ],
      [ "Deprecated List", "deprecated.html", null ]
    ] ],
    [ "Modules", "modules.html", [
      [ "Callback function typedefs", "group__callback__functions.html", null ],
      [ "Windows handling functions", "group__fl__windows.html", null ],
      [ "Events handling functions", "group__fl__events.html", null ],
      [ "Selection & Clipboard functions", "group__fl__clipboard.html", null ],
      [ "Screen functions", "group__fl__screen.html", null ],
      [ "Color & Font functions", "group__fl__attributes.html", null ],
      [ "Drawing functions", "group__fl__drawings.html", null ],
      [ "Multithreading support functions", "group__fl__multithread.html", null ],
      [ "Safe widget deletion support functions", "group__fl__del__widget.html", null ],
      [ "Cairo support functions and classes", "group__group__cairo.html", null ],
      [ "Unicode and UTF-8 functions", "group__fl__unicode.html", null ],
      [ "Mac OS X-specific symbols", "group__group__macosx.html", null ],
      [ "Common Dialogs classes and functions", "group__group__comdlg.html", null ],
      [ "File names and URI utility functions", "group__filenames.html", null ]
    ] ],
    [ "Class List", "annotated.html", [
      [ "Fl_Preferences::Entry", "structFl__Preferences_1_1Entry.html", null ],
      [ "Fl", "classFl.html", null ],
      [ "Fl_Adjuster", "classFl__Adjuster.html", null ],
      [ "Fl_Bitmap", "classFl__Bitmap.html", null ],
      [ "Fl_BMP_Image", "classFl__BMP__Image.html", null ],
      [ "Fl_Box", "classFl__Box.html", null ],
      [ "Fl_Browser", "classFl__Browser.html", null ],
      [ "Fl_Browser_", "classFl__Browser__.html", null ],
      [ "Fl_Button", "classFl__Button.html", null ],
      [ "Fl_Cairo_State", "classFl__Cairo__State.html", null ],
      [ "Fl_Cairo_Window", "classFl__Cairo__Window.html", null ],
      [ "Fl_Chart", "classFl__Chart.html", null ],
      [ "FL_CHART_ENTRY", "structFL__CHART__ENTRY.html", null ],
      [ "Fl_Check_Browser", "classFl__Check__Browser.html", null ],
      [ "Fl_Check_Button", "classFl__Check__Button.html", null ],
      [ "Fl_Choice", "classFl__Choice.html", null ],
      [ "Fl_Clock", "classFl__Clock.html", null ],
      [ "Fl_Clock_Output", "classFl__Clock__Output.html", null ],
      [ "Fl_Color_Chooser", "classFl__Color__Chooser.html", null ],
      [ "Fl_Counter", "classFl__Counter.html", null ],
      [ "Fl_Device", "classFl__Device.html", null ],
      [ "Fl_Device_Plugin", "classFl__Device__Plugin.html", null ],
      [ "Fl_Dial", "classFl__Dial.html", null ],
      [ "Fl_Display_Device", "classFl__Display__Device.html", null ],
      [ "Fl_Double_Window", "classFl__Double__Window.html", null ],
      [ "Fl_End", "classFl__End.html", null ],
      [ "Fl_File_Browser", "classFl__File__Browser.html", null ],
      [ "Fl_File_Chooser", "classFl__File__Chooser.html", null ],
      [ "Fl_File_Icon", "classFl__File__Icon.html", null ],
      [ "Fl_File_Input", "classFl__File__Input.html", null ],
      [ "Fl_Fill_Dial", "classFl__Fill__Dial.html", null ],
      [ "Fl_Fill_Slider", "classFl__Fill__Slider.html", null ],
      [ "Fl_Float_Input", "classFl__Float__Input.html", null ],
      [ "Fl_Font_Descriptor", "classFl__Font__Descriptor.html", null ],
      [ "Fl_Fontdesc", "structFl__Fontdesc.html", null ],
      [ "Fl_FormsBitmap", "classFl__FormsBitmap.html", null ],
      [ "Fl_FormsPixmap", "classFl__FormsPixmap.html", null ],
      [ "Fl_Free", "classFl__Free.html", null ],
      [ "Fl_GDI_Graphics_Driver", "classFl__GDI__Graphics__Driver.html", null ],
      [ "Fl_GDI_Printer_Graphics_Driver", "classFl__GDI__Printer__Graphics__Driver.html", null ],
      [ "Fl_GIF_Image", "classFl__GIF__Image.html", null ],
      [ "Fl_Gl_Choice", "classFl__Gl__Choice.html", null ],
      [ "Fl_Gl_Window", "classFl__Gl__Window.html", null ],
      [ "Fl_Glut_Bitmap_Font", "structFl__Glut__Bitmap__Font.html", null ],
      [ "Fl_Glut_StrokeChar", "structFl__Glut__StrokeChar.html", null ],
      [ "Fl_Glut_StrokeFont", "structFl__Glut__StrokeFont.html", null ],
      [ "Fl_Glut_StrokeStrip", "structFl__Glut__StrokeStrip.html", null ],
      [ "Fl_Glut_StrokeVertex", "structFl__Glut__StrokeVertex.html", null ],
      [ "Fl_Glut_Window", "classFl__Glut__Window.html", null ],
      [ "Fl_Graphics_Driver", "classFl__Graphics__Driver.html", null ],
      [ "Fl_Group", "classFl__Group.html", null ],
      [ "Fl_Help_Block", "structFl__Help__Block.html", null ],
      [ "Fl_Help_Dialog", "classFl__Help__Dialog.html", null ],
      [ "Fl_Help_Font_Stack", "structFl__Help__Font__Stack.html", null ],
      [ "Fl_Help_Font_Style", "structFl__Help__Font__Style.html", null ],
      [ "Fl_Help_Link", "structFl__Help__Link.html", null ],
      [ "Fl_Help_Target", "structFl__Help__Target.html", null ],
      [ "Fl_Help_View", "classFl__Help__View.html", null ],
      [ "Fl_Hold_Browser", "classFl__Hold__Browser.html", null ],
      [ "Fl_Hor_Fill_Slider", "classFl__Hor__Fill__Slider.html", null ],
      [ "Fl_Hor_Nice_Slider", "classFl__Hor__Nice__Slider.html", null ],
      [ "Fl_Hor_Slider", "classFl__Hor__Slider.html", null ],
      [ "Fl_Hor_Value_Slider", "classFl__Hor__Value__Slider.html", null ],
      [ "Fl_Image", "classFl__Image.html", null ],
      [ "Fl_Input", "classFl__Input.html", null ],
      [ "Fl_Input_", "classFl__Input__.html", null ],
      [ "Fl_Input_Choice", "classFl__Input__Choice.html", null ],
      [ "Fl_Int_Input", "classFl__Int__Input.html", null ],
      [ "Fl_JPEG_Image", "classFl__JPEG__Image.html", null ],
      [ "Fl_Label", "structFl__Label.html", null ],
      [ "Fl_Light_Button", "classFl__Light__Button.html", null ],
      [ "Fl_Line_Dial", "classFl__Line__Dial.html", null ],
      [ "Fl_Mac_App_Menu", "classFl__Mac__App__Menu.html", null ],
      [ "Fl_Menu_", "classFl__Menu__.html", null ],
      [ "Fl_Menu_Bar", "classFl__Menu__Bar.html", null ],
      [ "Fl_Menu_Button", "classFl__Menu__Button.html", null ],
      [ "Fl_Menu_Item", "structFl__Menu__Item.html", null ],
      [ "Fl_Menu_Window", "classFl__Menu__Window.html", null ],
      [ "Fl_Multi_Browser", "classFl__Multi__Browser.html", null ],
      [ "Fl_Multi_Label", "structFl__Multi__Label.html", null ],
      [ "Fl_Multiline_Input", "classFl__Multiline__Input.html", null ],
      [ "Fl_Multiline_Output", "classFl__Multiline__Output.html", null ],
      [ "Fl_Native_File_Chooser", "classFl__Native__File__Chooser.html", null ],
      [ "Fl_Nice_Slider", "classFl__Nice__Slider.html", null ],
      [ "Fl_Output", "classFl__Output.html", null ],
      [ "Fl_Overlay_Window", "classFl__Overlay__Window.html", null ],
      [ "Fl_Pack", "classFl__Pack.html", null ],
      [ "Fl_Paged_Device", "classFl__Paged__Device.html", null ],
      [ "Fl_Pixmap", "classFl__Pixmap.html", null ],
      [ "Fl_Plugin", "classFl__Plugin.html", null ],
      [ "Fl_Plugin_Manager", "classFl__Plugin__Manager.html", null ],
      [ "Fl_PNG_Image", "classFl__PNG__Image.html", null ],
      [ "Fl_PNM_Image", "classFl__PNM__Image.html", null ],
      [ "Fl_Positioner", "classFl__Positioner.html", null ],
      [ "Fl_PostScript_File_Device", "classFl__PostScript__File__Device.html", null ],
      [ "Fl_PostScript_Graphics_Driver", "classFl__PostScript__Graphics__Driver.html", null ],
      [ "Fl_PostScript_Printer", "classFl__PostScript__Printer.html", null ],
      [ "Fl_Preferences", "classFl__Preferences.html", null ],
      [ "Fl_Printer", "classFl__Printer.html", null ],
      [ "Fl_Progress", "classFl__Progress.html", null ],
      [ "Fl_Quartz_Graphics_Driver", "classFl__Quartz__Graphics__Driver.html", null ],
      [ "Fl_Radio_Button", "classFl__Radio__Button.html", null ],
      [ "Fl_Radio_Light_Button", "classFl__Radio__Light__Button.html", null ],
      [ "Fl_Radio_Round_Button", "classFl__Radio__Round__Button.html", null ],
      [ "Fl_Repeat_Button", "classFl__Repeat__Button.html", null ],
      [ "Fl_Return_Button", "classFl__Return__Button.html", null ],
      [ "Fl_RGB_Image", "classFl__RGB__Image.html", null ],
      [ "Fl_Roller", "classFl__Roller.html", null ],
      [ "Fl_Round_Button", "classFl__Round__Button.html", null ],
      [ "Fl_Round_Clock", "classFl__Round__Clock.html", null ],
      [ "Fl_Scroll", "classFl__Scroll.html", null ],
      [ "Fl_Scrollbar", "classFl__Scrollbar.html", null ],
      [ "Fl_Secret_Input", "classFl__Secret__Input.html", null ],
      [ "Fl_Select_Browser", "classFl__Select__Browser.html", null ],
      [ "Fl_Shared_Image", "classFl__Shared__Image.html", null ],
      [ "Fl_Simple_Counter", "classFl__Simple__Counter.html", null ],
      [ "Fl_Single_Window", "classFl__Single__Window.html", null ],
      [ "Fl_Slider", "classFl__Slider.html", null ],
      [ "Fl_Spinner", "classFl__Spinner.html", null ],
      [ "Fl_Surface_Device", "classFl__Surface__Device.html", null ],
      [ "Fl_Sys_Menu_Bar", "classFl__Sys__Menu__Bar.html", null ],
      [ "Fl_System_Printer", "classFl__System__Printer.html", null ],
      [ "Fl_Table", "classFl__Table.html", null ],
      [ "Fl_Table_Row", "classFl__Table__Row.html", null ],
      [ "Fl_Tabs", "classFl__Tabs.html", null ],
      [ "Fl_Text_Buffer", "classFl__Text__Buffer.html", null ],
      [ "Fl_Text_Display", "classFl__Text__Display.html", null ],
      [ "Fl_Text_Editor", "classFl__Text__Editor.html", null ],
      [ "Fl_Text_Selection", "classFl__Text__Selection.html", null ],
      [ "Fl_Tile", "classFl__Tile.html", null ],
      [ "Fl_Tiled_Image", "classFl__Tiled__Image.html", null ],
      [ "Fl_Timer", "classFl__Timer.html", null ],
      [ "Fl_Toggle_Button", "classFl__Toggle__Button.html", null ],
      [ "Fl_Tooltip", "classFl__Tooltip.html", null ],
      [ "Fl_Tree", "classFl__Tree.html", null ],
      [ "Fl_Tree_Item", "classFl__Tree__Item.html", null ],
      [ "Fl_Tree_Item_Array", "classFl__Tree__Item__Array.html", null ],
      [ "Fl_Tree_Prefs", "classFl__Tree__Prefs.html", null ],
      [ "Fl_Valuator", "classFl__Valuator.html", null ],
      [ "Fl_Value_Input", "classFl__Value__Input.html", null ],
      [ "Fl_Value_Output", "classFl__Value__Output.html", null ],
      [ "Fl_Value_Slider", "classFl__Value__Slider.html", null ],
      [ "Fl_Widget", "classFl__Widget.html", null ],
      [ "Fl_Widget_Tracker", "classFl__Widget__Tracker.html", null ],
      [ "Fl_Window", "classFl__Window.html", null ],
      [ "Fl_Wizard", "classFl__Wizard.html", null ],
      [ "Fl_XBM_Image", "classFl__XBM__Image.html", null ],
      [ "Fl_XColor", "structFl__XColor.html", null ],
      [ "Fl_Xlib_Graphics_Driver", "classFl__Xlib__Graphics__Driver.html", null ],
      [ "Fl_XPM_Image", "classFl__XPM__Image.html", null ],
      [ "FLMenuItem", "interfaceFLMenuItem.html", null ],
      [ "IActiveIMMApp", "classIActiveIMMApp.html", null ],
      [ "Fl_Text_Editor::Key_Binding", "structFl__Text__Editor_1_1Key__Binding.html", null ],
      [ "Fl_Graphics_Driver::matrix", "structFl__Graphics__Driver_1_1matrix.html", null ],
      [ "Fl_Preferences::Name", "classFl__Preferences_1_1Name.html", null ],
      [ "Fl_Paged_Device::page_format", "structFl__Paged__Device_1_1page__format.html", null ],
      [ "Fl_Text_Display::Style_Table_Entry", "structFl__Text__Display_1_1Style__Table__Entry.html", null ],
      [ "XUtf8FontStruct", "structXUtf8FontStruct.html", null ]
    ] ],
    [ "Class Index", "classes.html", null ],
    [ "Class Hierarchy", "hierarchy.html", [
      [ "Fl_Preferences::Entry", "structFl__Preferences_1_1Entry.html", null ],
      [ "Fl", "classFl.html", null ],
      [ "Fl_Cairo_State", "classFl__Cairo__State.html", null ],
      [ "FL_CHART_ENTRY", "structFL__CHART__ENTRY.html", null ],
      [ "Fl_Device", "classFl__Device.html", [
        [ "Fl_Graphics_Driver", "classFl__Graphics__Driver.html", [
          [ "Fl_GDI_Graphics_Driver", "classFl__GDI__Graphics__Driver.html", [
            [ "Fl_GDI_Printer_Graphics_Driver", "classFl__GDI__Printer__Graphics__Driver.html", null ]
          ] ],
          [ "Fl_PostScript_Graphics_Driver", "classFl__PostScript__Graphics__Driver.html", null ],
          [ "Fl_Quartz_Graphics_Driver", "classFl__Quartz__Graphics__Driver.html", null ],
          [ "Fl_Xlib_Graphics_Driver", "classFl__Xlib__Graphics__Driver.html", null ]
        ] ],
        [ "Fl_Surface_Device", "classFl__Surface__Device.html", [
          [ "Fl_Display_Device", "classFl__Display__Device.html", null ],
          [ "Fl_Paged_Device", "classFl__Paged__Device.html", [
            [ "Fl_PostScript_File_Device", "classFl__PostScript__File__Device.html", [
              [ "Fl_PostScript_Printer", "classFl__PostScript__Printer.html", null ]
            ] ],
            [ "Fl_Printer", "classFl__Printer.html", null ],
            [ "Fl_System_Printer", "classFl__System__Printer.html", null ]
          ] ]
        ] ]
      ] ],
      [ "Fl_End", "classFl__End.html", null ],
      [ "Fl_File_Chooser", "classFl__File__Chooser.html", null ],
      [ "Fl_File_Icon", "classFl__File__Icon.html", null ],
      [ "Fl_Font_Descriptor", "classFl__Font__Descriptor.html", null ],
      [ "Fl_Fontdesc", "structFl__Fontdesc.html", null ],
      [ "Fl_Gl_Choice", "classFl__Gl__Choice.html", null ],
      [ "Fl_Glut_Bitmap_Font", "structFl__Glut__Bitmap__Font.html", null ],
      [ "Fl_Glut_StrokeChar", "structFl__Glut__StrokeChar.html", null ],
      [ "Fl_Glut_StrokeFont", "structFl__Glut__StrokeFont.html", null ],
      [ "Fl_Glut_StrokeStrip", "structFl__Glut__StrokeStrip.html", null ],
      [ "Fl_Glut_StrokeVertex", "structFl__Glut__StrokeVertex.html", null ],
      [ "Fl_Help_Block", "structFl__Help__Block.html", null ],
      [ "Fl_Help_Dialog", "classFl__Help__Dialog.html", null ],
      [ "Fl_Help_Font_Stack", "structFl__Help__Font__Stack.html", null ],
      [ "Fl_Help_Font_Style", "structFl__Help__Font__Style.html", null ],
      [ "Fl_Help_Link", "structFl__Help__Link.html", null ],
      [ "Fl_Help_Target", "structFl__Help__Target.html", null ],
      [ "Fl_Image", "classFl__Image.html", [
        [ "Fl_Bitmap", "classFl__Bitmap.html", [
          [ "Fl_XBM_Image", "classFl__XBM__Image.html", null ]
        ] ],
        [ "Fl_Pixmap", "classFl__Pixmap.html", [
          [ "Fl_GIF_Image", "classFl__GIF__Image.html", null ],
          [ "Fl_XPM_Image", "classFl__XPM__Image.html", null ]
        ] ],
        [ "Fl_RGB_Image", "classFl__RGB__Image.html", [
          [ "Fl_BMP_Image", "classFl__BMP__Image.html", null ],
          [ "Fl_JPEG_Image", "classFl__JPEG__Image.html", null ],
          [ "Fl_PNG_Image", "classFl__PNG__Image.html", null ],
          [ "Fl_PNM_Image", "classFl__PNM__Image.html", null ]
        ] ],
        [ "Fl_Shared_Image", "classFl__Shared__Image.html", null ],
        [ "Fl_Tiled_Image", "classFl__Tiled__Image.html", null ]
      ] ],
      [ "Fl_Label", "structFl__Label.html", null ],
      [ "Fl_Mac_App_Menu", "classFl__Mac__App__Menu.html", null ],
      [ "Fl_Menu_Item", "structFl__Menu__Item.html", null ],
      [ "Fl_Multi_Label", "structFl__Multi__Label.html", null ],
      [ "Fl_Native_File_Chooser", "classFl__Native__File__Chooser.html", null ],
      [ "Fl_Plugin", "classFl__Plugin.html", [
        [ "Fl_Device_Plugin", "classFl__Device__Plugin.html", null ]
      ] ],
      [ "Fl_Preferences", "classFl__Preferences.html", [
        [ "Fl_Plugin_Manager", "classFl__Plugin__Manager.html", null ]
      ] ],
      [ "Fl_Text_Buffer", "classFl__Text__Buffer.html", null ],
      [ "Fl_Text_Selection", "classFl__Text__Selection.html", null ],
      [ "Fl_Tooltip", "classFl__Tooltip.html", null ],
      [ "Fl_Tree_Item", "classFl__Tree__Item.html", null ],
      [ "Fl_Tree_Item_Array", "classFl__Tree__Item__Array.html", null ],
      [ "Fl_Tree_Prefs", "classFl__Tree__Prefs.html", null ],
      [ "Fl_Widget", "classFl__Widget.html", [
        [ "Fl_Box", "classFl__Box.html", null ],
        [ "Fl_Button", "classFl__Button.html", [
          [ "Fl_Light_Button", "classFl__Light__Button.html", [
            [ "Fl_Check_Button", "classFl__Check__Button.html", null ],
            [ "Fl_Radio_Light_Button", "classFl__Radio__Light__Button.html", null ],
            [ "Fl_Round_Button", "classFl__Round__Button.html", [
              [ "Fl_Radio_Round_Button", "classFl__Radio__Round__Button.html", null ]
            ] ]
          ] ],
          [ "Fl_Radio_Button", "classFl__Radio__Button.html", null ],
          [ "Fl_Repeat_Button", "classFl__Repeat__Button.html", null ],
          [ "Fl_Return_Button", "classFl__Return__Button.html", null ],
          [ "Fl_Toggle_Button", "classFl__Toggle__Button.html", null ]
        ] ],
        [ "Fl_Chart", "classFl__Chart.html", null ],
        [ "Fl_Clock_Output", "classFl__Clock__Output.html", [
          [ "Fl_Clock", "classFl__Clock.html", [
            [ "Fl_Round_Clock", "classFl__Round__Clock.html", null ]
          ] ]
        ] ],
        [ "Fl_FormsBitmap", "classFl__FormsBitmap.html", null ],
        [ "Fl_FormsPixmap", "classFl__FormsPixmap.html", null ],
        [ "Fl_Free", "classFl__Free.html", null ],
        [ "Fl_Group", "classFl__Group.html", [
          [ "Fl_Browser_", "classFl__Browser__.html", [
            [ "Fl_Browser", "classFl__Browser.html", [
              [ "Fl_File_Browser", "classFl__File__Browser.html", null ],
              [ "Fl_Hold_Browser", "classFl__Hold__Browser.html", null ],
              [ "Fl_Multi_Browser", "classFl__Multi__Browser.html", null ],
              [ "Fl_Select_Browser", "classFl__Select__Browser.html", null ]
            ] ],
            [ "Fl_Check_Browser", "classFl__Check__Browser.html", null ]
          ] ],
          [ "Fl_Color_Chooser", "classFl__Color__Chooser.html", null ],
          [ "Fl_Help_View", "classFl__Help__View.html", null ],
          [ "Fl_Input_Choice", "classFl__Input__Choice.html", null ],
          [ "Fl_Pack", "classFl__Pack.html", null ],
          [ "Fl_Scroll", "classFl__Scroll.html", null ],
          [ "Fl_Spinner", "classFl__Spinner.html", null ],
          [ "Fl_Table", "classFl__Table.html", [
            [ "Fl_Table_Row", "classFl__Table__Row.html", null ]
          ] ],
          [ "Fl_Tabs", "classFl__Tabs.html", null ],
          [ "Fl_Text_Display", "classFl__Text__Display.html", [
            [ "Fl_Text_Editor", "classFl__Text__Editor.html", null ]
          ] ],
          [ "Fl_Tile", "classFl__Tile.html", null ],
          [ "Fl_Tree", "classFl__Tree.html", null ],
          [ "Fl_Window", "classFl__Window.html", [
            [ "Fl_Double_Window", "classFl__Double__Window.html", [
              [ "Fl_Cairo_Window", "classFl__Cairo__Window.html", null ],
              [ "Fl_Overlay_Window", "classFl__Overlay__Window.html", null ]
            ] ],
            [ "Fl_Gl_Window", "classFl__Gl__Window.html", [
              [ "Fl_Glut_Window", "classFl__Glut__Window.html", null ]
            ] ],
            [ "Fl_Single_Window", "classFl__Single__Window.html", [
              [ "Fl_Menu_Window", "classFl__Menu__Window.html", null ]
            ] ]
          ] ],
          [ "Fl_Wizard", "classFl__Wizard.html", null ]
        ] ],
        [ "Fl_Input_", "classFl__Input__.html", [
          [ "Fl_Input", "classFl__Input.html", [
            [ "Fl_File_Input", "classFl__File__Input.html", null ],
            [ "Fl_Float_Input", "classFl__Float__Input.html", null ],
            [ "Fl_Int_Input", "classFl__Int__Input.html", null ],
            [ "Fl_Multiline_Input", "classFl__Multiline__Input.html", null ],
            [ "Fl_Output", "classFl__Output.html", [
              [ "Fl_Multiline_Output", "classFl__Multiline__Output.html", null ]
            ] ],
            [ "Fl_Secret_Input", "classFl__Secret__Input.html", null ]
          ] ]
        ] ],
        [ "Fl_Menu_", "classFl__Menu__.html", [
          [ "Fl_Choice", "classFl__Choice.html", null ],
          [ "Fl_Menu_Bar", "classFl__Menu__Bar.html", [
            [ "Fl_Sys_Menu_Bar", "classFl__Sys__Menu__Bar.html", null ]
          ] ],
          [ "Fl_Menu_Button", "classFl__Menu__Button.html", null ]
        ] ],
        [ "Fl_Positioner", "classFl__Positioner.html", null ],
        [ "Fl_Progress", "classFl__Progress.html", null ],
        [ "Fl_Timer", "classFl__Timer.html", null ],
        [ "Fl_Valuator", "classFl__Valuator.html", [
          [ "Fl_Adjuster", "classFl__Adjuster.html", null ],
          [ "Fl_Counter", "classFl__Counter.html", [
            [ "Fl_Simple_Counter", "classFl__Simple__Counter.html", null ]
          ] ],
          [ "Fl_Dial", "classFl__Dial.html", [
            [ "Fl_Fill_Dial", "classFl__Fill__Dial.html", null ],
            [ "Fl_Line_Dial", "classFl__Line__Dial.html", null ]
          ] ],
          [ "Fl_Roller", "classFl__Roller.html", null ],
          [ "Fl_Slider", "classFl__Slider.html", [
            [ "Fl_Fill_Slider", "classFl__Fill__Slider.html", null ],
            [ "Fl_Hor_Fill_Slider", "classFl__Hor__Fill__Slider.html", null ],
            [ "Fl_Hor_Nice_Slider", "classFl__Hor__Nice__Slider.html", null ],
            [ "Fl_Hor_Slider", "classFl__Hor__Slider.html", null ],
            [ "Fl_Nice_Slider", "classFl__Nice__Slider.html", null ],
            [ "Fl_Scrollbar", "classFl__Scrollbar.html", null ],
            [ "Fl_Value_Slider", "classFl__Value__Slider.html", [
              [ "Fl_Hor_Value_Slider", "classFl__Hor__Value__Slider.html", null ]
            ] ]
          ] ],
          [ "Fl_Value_Input", "classFl__Value__Input.html", null ],
          [ "Fl_Value_Output", "classFl__Value__Output.html", null ]
        ] ]
      ] ],
      [ "Fl_Widget_Tracker", "classFl__Widget__Tracker.html", null ],
      [ "Fl_XColor", "structFl__XColor.html", null ],
      [ "FLMenuItem", "interfaceFLMenuItem.html", null ],
      [ "IActiveIMMApp", "classIActiveIMMApp.html", null ],
      [ "Fl_Text_Editor::Key_Binding", "structFl__Text__Editor_1_1Key__Binding.html", null ],
      [ "Fl_Graphics_Driver::matrix", "structFl__Graphics__Driver_1_1matrix.html", null ],
      [ "Fl_Preferences::Name", "classFl__Preferences_1_1Name.html", null ],
      [ "Fl_Paged_Device::page_format", "structFl__Paged__Device_1_1page__format.html", null ],
      [ "Fl_Text_Display::Style_Table_Entry", "structFl__Text__Display_1_1Style__Table__Entry.html", null ],
      [ "XUtf8FontStruct", "structXUtf8FontStruct.html", null ]
    ] ],
    [ "Class Members", "functions.html", null ],
    [ "File List", "files.html", [
      [ "aimm.h", null, null ],
      [ "armscii_8.h", null, null ],
      [ "ascii.h", null, null ],
      [ "big5.h", null, null ],
      [ "big5_emacs.h", null, null ],
      [ "case.h", null, null ],
      [ "cgdebug.h", null, null ],
      [ "cp1133.h", null, null ],
      [ "cp1251.h", null, null ],
      [ "cp1255.h", null, null ],
      [ "cp1256.h", null, null ],
      [ "cp936ext.h", null, null ],
      [ "dingbats_.h", null, null ],
      [ "dirent.h", null, null ],
      [ "Enumerations.H", "Enumerations_8H.html", null ],
      [ "fastarrow.h", null, null ],
      [ "filename.H", "filename_8H.html", null ],
      [ "Fl.H", "Fl_8H.html", null ],
      [ "Fl_Adjuster.H", null, null ],
      [ "fl_arc.cxx", "fl__arc_8cxx.html", null ],
      [ "fl_arci.cxx", "fl__arci_8cxx.html", null ],
      [ "fl_ask.cxx", "fl__ask_8cxx.html", null ],
      [ "fl_ask.H", "fl__ask_8H.html", null ],
      [ "Fl_Bitmap.H", null, null ],
      [ "Fl_BMP_Image.H", null, null ],
      [ "Fl_Box.H", null, null ],
      [ "fl_boxtype.cxx", "fl__boxtype_8cxx.html", null ],
      [ "Fl_Browser.H", null, null ],
      [ "Fl_Browser_.H", null, null ],
      [ "Fl_Button.H", null, null ],
      [ "Fl_Cairo.H", null, null ],
      [ "Fl_Cairo_Window.H", null, null ],
      [ "Fl_Chart.H", null, null ],
      [ "Fl_Check_Browser.H", null, null ],
      [ "Fl_Check_Button.H", null, null ],
      [ "Fl_Choice.H", null, null ],
      [ "Fl_Clock.H", null, null ],
      [ "fl_cmap.h", null, null ],
      [ "fl_color.cxx", "fl__color_8cxx.html", null ],
      [ "Fl_Color_Chooser.H", "Fl__Color__Chooser_8H.html", null ],
      [ "Fl_compose.cxx", "Fl__compose_8cxx.html", null ],
      [ "Fl_Counter.H", null, null ],
      [ "fl_curve.cxx", "fl__curve_8cxx.html", null ],
      [ "Fl_Device.H", "Fl__Device_8H.html", null ],
      [ "Fl_Dial.H", null, null ],
      [ "Fl_Double_Window.cxx", "Fl__Double__Window_8cxx.html", null ],
      [ "Fl_Double_Window.H", null, null ],
      [ "fl_draw.H", "fl__draw_8H.html", null ],
      [ "Fl_Export.H", null, null ],
      [ "Fl_File_Browser.H", null, null ],
      [ "Fl_File_Chooser.H", null, null ],
      [ "Fl_File_Icon.H", null, null ],
      [ "Fl_File_Input.H", null, null ],
      [ "Fl_Fill_Dial.H", null, null ],
      [ "Fl_Fill_Slider.H", null, null ],
      [ "Fl_Float_Input.H", null, null ],
      [ "Fl_Font.H", null, null ],
      [ "Fl_FormsBitmap.H", null, null ],
      [ "Fl_FormsPixmap.H", null, null ],
      [ "Fl_Free.H", null, null ],
      [ "Fl_GIF_Image.H", null, null ],
      [ "Fl_Gl_Choice.H", null, null ],
      [ "Fl_Gl_Window.H", null, null ],
      [ "Fl_Group.H", null, null ],
      [ "Fl_Help_Dialog.H", null, null ],
      [ "Fl_Help_View.H", null, null ],
      [ "Fl_Hold_Browser.H", null, null ],
      [ "Fl_Hor_Fill_Slider.H", null, null ],
      [ "Fl_Hor_Nice_Slider.H", null, null ],
      [ "Fl_Hor_Slider.H", null, null ],
      [ "Fl_Hor_Value_Slider.H", null, null ],
      [ "Fl_Image.H", null, null ],
      [ "Fl_Input.H", null, null ],
      [ "Fl_Input_.H", null, null ],
      [ "Fl_Input_Choice.H", null, null ],
      [ "Fl_Int_Input.H", null, null ],
      [ "Fl_JPEG_Image.H", null, null ],
      [ "Fl_Light_Button.H", null, null ],
      [ "Fl_Line_Dial.H", null, null ],
      [ "fl_line_style.cxx", "fl__line__style_8cxx.html", null ],
      [ "Fl_Menu.H", null, null ],
      [ "Fl_Menu_.H", null, null ],
      [ "Fl_Menu_Bar.H", null, null ],
      [ "Fl_Menu_Button.H", null, null ],
      [ "Fl_Menu_Item.H", null, null ],
      [ "Fl_Menu_Window.H", null, null ],
      [ "fl_message.H", null, null ],
      [ "Fl_Multi_Browser.H", null, null ],
      [ "Fl_Multi_Label.H", null, null ],
      [ "Fl_Multiline_Input.H", null, null ],
      [ "Fl_Multiline_Output.H", null, null ],
      [ "Fl_Native_File_Chooser.H", "Fl__Native__File__Chooser_8H.html", null ],
      [ "Fl_Nice_Slider.H", null, null ],
      [ "Fl_Object.H", null, null ],
      [ "Fl_Output.H", null, null ],
      [ "Fl_Overlay_Window.H", null, null ],
      [ "Fl_Pack.H", null, null ],
      [ "Fl_Paged_Device.cxx", "Fl__Paged__Device_8cxx.html", null ],
      [ "Fl_Paged_Device.H", "Fl__Paged__Device_8H.html", null ],
      [ "Fl_Pixmap.H", null, null ],
      [ "Fl_Plugin.H", null, null ],
      [ "Fl_PNG_Image.H", null, null ],
      [ "Fl_PNM_Image.H", null, null ],
      [ "Fl_Positioner.H", null, null ],
      [ "Fl_PostScript.H", "Fl__PostScript_8H.html", null ],
      [ "Fl_Preferences.H", null, null ],
      [ "Fl_Printer.H", "Fl__Printer_8H.html", null ],
      [ "Fl_Progress.H", null, null ],
      [ "Fl_Radio_Button.H", null, null ],
      [ "Fl_Radio_Light_Button.H", null, null ],
      [ "Fl_Radio_Round_Button.H", null, null ],
      [ "fl_rect.cxx", "fl__rect_8cxx.html", null ],
      [ "Fl_Repeat_Button.H", null, null ],
      [ "Fl_Return_Button.H", null, null ],
      [ "Fl_RGB_Image.H", null, null ],
      [ "Fl_Roller.H", null, null ],
      [ "Fl_Round_Button.H", null, null ],
      [ "Fl_Round_Clock.H", null, null ],
      [ "Fl_Scroll.H", null, null ],
      [ "Fl_Scrollbar.H", null, null ],
      [ "Fl_Secret_Input.H", null, null ],
      [ "Fl_Select_Browser.H", null, null ],
      [ "Fl_Shared_Image.H", "Fl__Shared__Image_8H.html", null ],
      [ "fl_show_colormap.H", "fl__show__colormap_8H.html", null ],
      [ "fl_show_input.H", null, null ],
      [ "Fl_Simple_Counter.H", null, null ],
      [ "Fl_Single_Window.H", null, null ],
      [ "Fl_Slider.H", null, null ],
      [ "Fl_Spinner.H", null, null ],
      [ "Fl_Sys_Menu_Bar.H", null, null ],
      [ "Fl_Table.H", null, null ],
      [ "Fl_Table_Row.H", null, null ],
      [ "Fl_Tabs.H", null, null ],
      [ "Fl_Text_Buffer.H", null, null ],
      [ "Fl_Text_Display.H", null, null ],
      [ "Fl_Text_Editor.H", null, null ],
      [ "Fl_Tile.H", null, null ],
      [ "Fl_Tiled_Image.H", null, null ],
      [ "Fl_Timer.H", null, null ],
      [ "Fl_Toggle_Button.H", null, null ],
      [ "Fl_Toggle_Light_Button.H", null, null ],
      [ "Fl_Toggle_Round_Button.H", null, null ],
      [ "Fl_Tooltip.H", null, null ],
      [ "Fl_Tree.H", "Fl__Tree_8H.html", null ],
      [ "Fl_Tree_Item.H", "Fl__Tree__Item_8H.html", null ],
      [ "Fl_Tree_Item_Array.H", "Fl__Tree__Item__Array_8H.html", null ],
      [ "Fl_Tree_Prefs.H", "Fl__Tree__Prefs_8H.html", null ],
      [ "fl_types.h", "fl__types_8h.html", null ],
      [ "fl_utf8.h", "fl__utf8_8h.html", null ],
      [ "Fl_Valuator.H", null, null ],
      [ "Fl_Value_Input.H", null, null ],
      [ "Fl_Value_Output.H", null, null ],
      [ "Fl_Value_Slider.H", null, null ],
      [ "fl_vertex.cxx", "fl__vertex_8cxx.html", null ],
      [ "Fl_Widget.H", "Fl__Widget_8H.html", null ],
      [ "Fl_Window.H", null, null ],
      [ "Fl_Wizard.H", null, null ],
      [ "Fl_XBM_Image.H", null, null ],
      [ "Fl_XColor.H", null, null ],
      [ "Fl_XPM_Image.H", null, null ],
      [ "flstring.h", null, null ],
      [ "forms.H", null, null ],
      [ "freeglut_teapot_data.h", null, null ],
      [ "gb2312.h", null, null ],
      [ "georgian_academy.h", null, null ],
      [ "georgian_ps.h", null, null ],
      [ "gl.h", "gl_8h.html", null ],
      [ "gl2opengl.h", null, null ],
      [ "gl_draw.H", null, null ],
      [ "glu.h", null, null ],
      [ "glut.H", null, null ],
      [ "iso8859_1.h", null, null ],
      [ "iso8859_10.h", null, null ],
      [ "iso8859_11.h", null, null ],
      [ "iso8859_13.h", null, null ],
      [ "iso8859_14.h", null, null ],
      [ "iso8859_15.h", null, null ],
      [ "iso8859_16.h", null, null ],
      [ "iso8859_2.h", null, null ],
      [ "iso8859_3.h", null, null ],
      [ "iso8859_4.h", null, null ],
      [ "iso8859_5.h", null, null ],
      [ "iso8859_6.h", null, null ],
      [ "iso8859_7.h", null, null ],
      [ "iso8859_8.h", null, null ],
      [ "iso8859_9.h", null, null ],
      [ "iso8859_9e.h", null, null ],
      [ "jisx0201.h", null, null ],
      [ "jisx0208.h", null, null ],
      [ "jisx0212.h", null, null ],
      [ "koi8_c.h", null, null ],
      [ "koi8_r.h", null, null ],
      [ "koi8_u.h", null, null ],
      [ "ksc5601.h", null, null ],
      [ "mac.H", "mac_8H.html", null ],
      [ "math.h", null, null ],
      [ "mediumarrow.h", null, null ],
      [ "mulelao.h", null, null ],
      [ "names.h", null, null ],
      [ "print_panel.h", null, null ],
      [ "slowarrow.h", null, null ],
      [ "spacing.h", null, null ],
      [ "symbol_.h", null, null ],
      [ "tatar_cyr.h", null, null ],
      [ "tcvn.h", null, null ],
      [ "tis620.h", null, null ],
      [ "ucs2be.h", null, null ],
      [ "utf8.h", null, null ],
      [ "viscii.h", null, null ],
      [ "win32.H", null, null ],
      [ "x.H", null, null ],
      [ "Ximint.h", null, null ],
      [ "Xlibint.h", null, null ],
      [ "Xutf8.h", null, null ]
    ] ],
    [ "File Members", "globals.html", null ]
  ] ]
];

function createIndent(o,domNode,node,level)
{
  if (node.parentNode && node.parentNode.parentNode)
  {
    createIndent(o,domNode,node.parentNode,level+1);
  }
  var imgNode = document.createElement("img");
  if (level==0 && node.childrenData)
  {
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() 
    {
      if (node.expanded) 
      {
        $(node.getChildrenUL()).slideUp("fast");
        if (node.isLast)
        {
          node.plus_img.src = node.relpath+"ftv2plastnode.png";
        }
        else
        {
          node.plus_img.src = node.relpath+"ftv2pnode.png";
        }
        node.expanded = false;
      } 
      else 
      {
        expandNode(o, node, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
  }
  else
  {
    domNode.appendChild(imgNode);
  }
  if (level==0)
  {
    if (node.isLast)
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2plastnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2lastnode.png";
        domNode.appendChild(imgNode);
      }
    }
    else
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2pnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2node.png";
        domNode.appendChild(imgNode);
      }
    }
  }
  else
  {
    if (node.isLast)
    {
      imgNode.src = node.relpath+"ftv2blank.png";
    }
    else
    {
      imgNode.src = node.relpath+"ftv2vertline.png";
    }
  }
  imgNode.border = "0";
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  a.appendChild(node.label);
  if (link) 
  {
    a.href = node.relpath+link;
  } 
  else 
  {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
      node.expanded = false;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() 
  {
    if (!node.childrenUL) 
    {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
}

function expandNode(o, node, imm)
{
  if (node.childrenData && !node.expanded) 
  {
    if (!node.childrenVisited) 
    {
      getNode(o, node);
    }
    if (imm)
    {
      $(node.getChildrenUL()).show();
    } 
    else 
    {
      $(node.getChildrenUL()).slideDown("fast",showRoot);
    }
    if (node.isLast)
    {
      node.plus_img.src = node.relpath+"ftv2mlastnode.png";
    }
    else
    {
      node.plus_img.src = node.relpath+"ftv2mnode.png";
    }
    node.expanded = true;
  }
}

function getNode(o, po)
{
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) 
  {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
        i==l);
  }
}

function findNavTreePage(url, data)
{
  var nodes = data;
  var result = null;
  for (var i in nodes) 
  {
    var d = nodes[i];
    if (d[1] == url) 
    {
      return new Array(i);
    }
    else if (d[2] != null) // array of children
    {
      result = findNavTreePage(url, d[2]);
      if (result != null) 
      {
        return (new Array(i).concat(result));
      }
    }
  }
  return null;
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;

  getNode(o, o.node);

  o.breadcrumbs = findNavTreePage(toroot, NAVTREE);
  if (o.breadcrumbs == null)
  {
    o.breadcrumbs = findNavTreePage("index.html",NAVTREE);
  }
  if (o.breadcrumbs != null && o.breadcrumbs.length>0)
  {
    var p = o.node;
    for (var i in o.breadcrumbs) 
    {
      var j = o.breadcrumbs[i];
      p = p.children[j];
      expandNode(o,p,true);
    }
    p.itemDiv.className = p.itemDiv.className + " selected";
    p.itemDiv.id = "selected";
    $(window).load(showRoot);
  }
}

