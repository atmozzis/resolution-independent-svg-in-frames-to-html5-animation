# README #

### What is this repository for? ###

* This project aims to create a program which generates html5-compatible animation from a set of SVG input frames. A set of SVG frames in sequence will be given as separate SVG files from which the output will be compiled and generated. C++ must be used for reusability of source code. The output animated file has to be compatible with modern web browsers and compliant with html5 standards.
* Version 1.0

### How do I get set up? ###

* Put exe file in SVG frames folder and run.

### Who do I talk to? ###

* Repo owner (atmozzis)